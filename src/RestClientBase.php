<?php

namespace GrzegorzUNITI;

class RestClientBase
{
    /**
     * cURL instance
     * @var instance
     */
    protected static $curl;
    
    /**
     * Configuration
     * @var array
     */
    protected static $config;
    
    public static function loadConfig( array $config )
    {
        self::$config = self::_validateConfig( $config );
    }
    
    public static function initializeRest( string $method = 'GET', string $url = null, $successFunction = null, $errorFunction = null )
    {
        if ( ! $url )
            throw new RestClientException( 'URL Address can not be empty!' );
            
        self::$curl = new Curl( $url );        
        self::$curl->setContentType( @self::$config['type'] );
        self::$curl->setRequestMethod( $method );
        
        if ( self::$config['auth'] )
        {
            self::$curl->authorization(
                new BasicAuthorization(
                    self::$config['auth']['login'],
                    self::$config['auth']['password']
                )
            );
        }
        
        if ( @self::$config['data'] )
        {
            self::$curl->{ 'assignData' . $method }( self::$config['data'] );
        }
        
        $response = self::$curl->execute();
        
        switch ( self::$curl->getStatusCode() )
        {
            case 200:
                if ( $successFunction )
                    call_user_func( $successFunction, [ $response ] );
                break;
            default:
                if ( $errorFunction )
                    call_user_func( $errorFunction, [ $response ] );
        }
    }
    
    protected static function _validateConfig( array $config )
    {
        if ( ! @$config['type'] )
            throw new RestClientException( 'You need to specify data type!' );
            
        return $config;
    }
}