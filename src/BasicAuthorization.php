<?php

namespace GrzegorzUNITI;

use GrzegorzUNITI\Abstracts\Authorization;
use GrzegorzUNITI\Interfaces\AuthorizationInterface;

class BasicAuthorization extends Authorization implements AuthorizationInterface
{
    public function __construct( string $login, string $password )
    {
        parent::__construct( $login, $password );
    }
    
    public function inject( $curl )
    {
        curl_setopt( $curl, CURLOPT_USERPWD, $this->login . ':' . $this->password );
    }
}