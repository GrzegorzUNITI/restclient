<?php

namespace GrzegorzUNITI\Abstracts;

abstract class Authorization
{
    /**
     * Login (username)
     * @var string
     */
    protected $login;
    
    /**
     * Password (not hashed!)
     * @var string
     */
    protected $password;
    
    public function __construct( string $login = null, string $password = null )
    {
        $this->login = $login;
        $this->password = $password;
    }
}