<?php

namespace GrzegorzUNITI;

use GrzegorzUNITI\Interfaces\RestClientInterface;

class RestClient extends RestClientBase implements RestClientInterface
{
    public static function GET( $url, $successFunction = null, $errorFunction = null, array $config = [] )
    {
        self::loadConfig( $config );
        self::initializeRest( 'GET', $url, $successFunction, $errorFunction );
    }
    
    public static function POST( $url, $successFunction = null, $errorFunction = null, array $config = [] )
    {
        self::loadConfig( $config );
        self::initializeRest( 'POST', $url, $successFunction, $errorFunction );
    }
    
    public static function PUT( $url, $successFunction = null, $errorFunction = null, array $config = [] ) { return; }
    public static function PATCH( $url, $successFunction = null, $errorFunction = null, array $config = [] ) { return; }
}