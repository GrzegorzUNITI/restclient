<?php

namespace GrzegorzUNITI\Interfaces;

interface RestClientInterface
{
    public static function GET( $url, $successFunction = null, $errorFunction = null, array $config = [] );
    public static function POST( $url, $successFunction = null, $errorFunction = null, array $config = [] );
    public static function PUT( $url, $successFunction = null, $errorFunction = null, array $config = [] );
    public static function PATCH( $url, $successFunction = null, $errorFunction = null, array $config = [] );
}