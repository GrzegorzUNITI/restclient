<?php

namespace GrzegorzUNITI;

class Curl
{
    /**
     * Curl instance
     * @var instance
     */
    protected $curl;
    
    protected $connectionTimeout = 20;
    protected $timeout = 20;
    
    public function __construct( string $url )
    {
        $this->curl = curl_init( $url );
        curl_setopt( $this->curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $this->curl, CURLOPT_CONNECTTIMEOUT, $this->connectionTimeout );
		curl_setopt( $this->curl, CURLOPT_TIMEOUT, $this->timeout );
    }
    
    public function authorization( $authorization )
    {
        $authorization->inject( $this->curl );
    }
    
    public function setContentType( string $type = 'json' )
    {
        curl_setopt( $this->curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/' . $type
        ]);
    }
    
    public function setRequestMethod( string $method = 'GET' )
    {
        curl_setopt( $this->curl, CURLOPT_CUSTOMREQUEST, $method );
    }
    
    public function getStatusCode()
    {
        return curl_getinfo( $this->curl, CURLINFO_HTTP_CODE );
    }
    
    public function assignDataPOST( array $data = [] )
    {
        curl_setopt( $this->curl, CURLOPT_POST, true );
        curl_setopt( $this->curl, CURLOPT_POSTFIELDS, json_encode( $data ) );
    }
    
    public function execute()
    {
        return curl_exec( $this->curl );
    }
    
    public function __destruct()
    {
        curl_close( $this->curl );
    }
}